module gitlab.com/mizanullkirom/latihan-mvc

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-chi/cors v1.2.0
	github.com/go-redis/redis/v8 v8.11.3
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang/mock v1.6.0
	github.com/joho/godotenv v1.4.0
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/smartystreets/goconvey v1.6.6 // indirect
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.7.2
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
