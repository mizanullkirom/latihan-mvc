package common

type AppCrypto interface {
	EncryptString(str string) ([]byte, error)
	CheckPassword(hashedPassword, password string) error
}
