package common

import "github.com/go-redis/redis/v8"

type RedisClient struct {
	client *redis.Client
}

var rdb *RedisClient

func RedisInit(address string, password string) *RedisClient {
	cl := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password, // no password set
		DB:       0,        // use default DB
	})
	rdb = &RedisClient{
		client: cl,
	}
	return rdb
}

func GetRedis() *redis.Client {
	return rdb.client
}
