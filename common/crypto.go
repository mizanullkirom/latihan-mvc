package common

import "golang.org/x/crypto/bcrypt"



type DefaultAppCrypto struct{}

// for Encrypt password
func (d DefaultAppCrypto) EncryptString(str string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(str), bcrypt.DefaultCost)
}

func (d DefaultAppCrypto) CheckPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}
