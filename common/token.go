package common

import (
	"fmt"
	"net/http"
	"strings"

	jwt "github.com/golang-jwt/jwt"
)

func GenerateToken(nik string) (string, error) {
	// nikEncrypt, err := EncryptString(nik)
	// if err != nil {
	// 	return "", err
	// }
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["nik"] = nik
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte("PersonSecret"))
}

func TokenValid(r *http.Request) error {
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte([]byte("PersonSecret")), nil
	})
	if err != nil {
		return err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		fmt.Println(claims)
	}
	return nil
}

func ExtractToken(r *http.Request) string {
	bearerToken := r.Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}
	return ""
}
