package controller_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/go-redis/redis/v8"
	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mizanullkirom/latihan-mvc/app"
	controller "gitlab.com/mizanullkirom/latihan-mvc/app/controllers"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository/mocks"
	"gitlab.com/mizanullkirom/latihan-mvc/app/usecase"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	mock "gitlab.com/mizanullkirom/latihan-mvc/common/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestAddItem(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	common.RedisInit("localhost:6379", "")

	var reqTest = []struct {
		bodyData     string
		expectedCode int
	}{
		{
			`{"nik": "1103130066","item" : "sepatu", "unit" : 5 }`,
			200,
		},
		{
			`{"nik": "1103130066","item" : "sepatu", "unit" : 15 }`,
			200,
		},
		{
			`{"nik": "","item" : "Sepatu", "unit" : 15 }}`,
			422,
		},
	}

	server := app.Server{}
	server.Initialize()
	for _, testData := range reqTest {
		bodyData := testData.bodyData
		req, err := http.NewRequest("POST", "/item", bytes.NewBufferString(bodyData))
		req.Header.Set("Content-Type", "application/json")
		asserts.NoError(err)

		w := httptest.NewRecorder()
		server.Router.ServeHTTP(w, req)

		asserts.Equal(testData.expectedCode, w.Code, "Respon")
	}
}

func TestDeleteItem(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	common.RedisInit("localhost:6379", "")

	var reqTest = []struct {
		nik          string
		item         string
		expectedCode int
	}{
		{
			"1103130066",
			"sepatu",
			200,
		},
		{
			"",
			"Sepatu",
			400,
		},
	}
	server := app.Server{}
	server.Initialize()
	for _, testData := range reqTest {
		nik := testData.nik
		item := testData.item
		req, err := http.NewRequest("DELETE", "/item/"+nik+"/"+item, nil)
		req.Header.Set("Content-Type", "application/json")
		asserts.NoError(err)

		w := httptest.NewRecorder()
		server.Router.ServeHTTP(w, req)
		asserts.Equal(testData.expectedCode, w.Code, err)
	}
}

func TestController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockPerson := mocks.NewMockIRepository(mockCtrl)
	mockCrypto := mock.NewMockAppCrypto(mockCtrl)
	defer mockCtrl.Finish()

	uc := &usecase.Usecase{
		Repo:   mockPerson,
		Crypto: mockCrypto,
	}
	ctrl := &controller.PersonController{UC: uc}

	r := chi.NewRouter()
	r.Route("/person", func(r chi.Router) {
		r.Get("/{nik}", ctrl.GetPersonController)
		r.Post("/", ctrl.AddPersonController)
		r.Delete("/{nik}", ctrl.DeletePersonController)
		r.Put("/{nik}", ctrl.UpdatePersonController)
	})

	Convey("AddPersonController", t, func() {
		Convey("NIK Empty", func() {
			bodyData := `{"nik": "","name" : "test", "phonenumber" : "085742666592", "address" : "test", "password" : "test" }`
			req, err := http.NewRequest("POST", "/person", bytes.NewBufferString(bodyData))
			req.Header.Set("Content-Type", "application/json")
			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)
			So(err, ShouldBeNil)
			ShouldEqual(w.Code, http.StatusUnprocessableEntity)
		})

		Convey("Person Exist", func() {
			bodyData := `{"nik": "11031","name" : "test", "phonenumber" : "085742666592", "address" : "test", "password" : "test" }`
			existingPerson := &models.Person{}
			mockPerson.EXPECT().GetPerson(existingPerson, bson.M{"nik": "11031"}).Return(nil).Times(1)
			req, err := http.NewRequest("POST", "/person", bytes.NewBufferString(bodyData))
			req.Header.Set("Content-Type", "application/json")
			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)
			fmt.Println(w.Code)
			So(err, ShouldBeNil)
			ShouldEqual(w.Code, http.StatusBadRequest)
		})

		Convey("Person Add", func() {
			bodyData := `{"nik": "11031","name" : "test", "phonenumber" : "085742666592", "address" : "test", "password" : "test" }`
			var person models.Person
			err := json.Unmarshal([]byte(bodyData), &person)
			if err != nil {
				fmt.Println(err)
				return
			}
			existingPerson := &models.Person{}
			mockPerson.EXPECT().GetPerson(existingPerson, bson.M{"nik": person.NIK}).Return(errors.New("error")).Times(1)
			mockCrypto.EXPECT().EncryptString(person.Password).Return([]byte(person.Password), nil).Times(1)
			mockPerson.EXPECT().SetPersonRedis(&person).Return(nil).Times(1)
			mockPerson.EXPECT().AddPerson(&person).Return(&mongo.InsertOneResult{}, nil).Times(1)
			req, err := http.NewRequest("POST", "/person", bytes.NewBufferString(bodyData))
			req.Header.Set("Content-Type", "application/json")
			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)
			So(err, ShouldBeNil)
			ShouldEqual(w.Code, http.StatusOK)
		})

	})

	Convey("GetPersonController", t, func() {
		nik := "11102"

		Convey("Get Person Empty NIK", func() {
			req, err := http.NewRequest("GET", fmt.Sprintf("/person/%s", ""), nil)
			req.Header.Set("Content-Type", "application/json")

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)
			So(err, ShouldBeNil)
			ShouldEqual(w.Code, http.StatusUnprocessableEntity)
		})

		Convey("Get Person Not Found", func() {
			person := &models.Person{}
			mockPerson.EXPECT().GetPersonRedis(nik).Return("", redis.Nil).Times(1)
			mockPerson.EXPECT().GetPerson(person, bson.M{"nik": nik}).Return(errors.New("error")).Times(1)

			req, err := http.NewRequest("GET", fmt.Sprintf("/person/%s", nik), nil)
			req.Header.Set("Content-Type", "application/json")

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)
			So(err, ShouldBeNil)
			ShouldEqual(w.Code, http.StatusBadRequest)
		})

	})

}
