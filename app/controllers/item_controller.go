package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
)

func (c *PersonController) AddItemController(w http.ResponseWriter, r *http.Request) {
	var item models.Item
	json.NewDecoder(r.Body).Decode(&item)
	if &item.NIK == nil || item.NIK == "" {
		http.Error(w, fmt.Sprintf("nik must required"), 422)
		return
	}
	val, err := c.UC.AddItem(&item)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte(val))
}

func (c *PersonController) DeleteItemController(w http.ResponseWriter, r *http.Request) {
	nik := chi.URLParam(r, "nik")
	item := chi.URLParam(r, "item")
	println(nik)
	if nik == "" || item == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	err := c.UC.DeleteItem(nik, item)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("item deleted"))
}
