package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/usecase"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
)

type PersonController struct {
	UC *usecase.Usecase
}

func (c *PersonController) GetPersonController(w http.ResponseWriter, r *http.Request) {
	nik := chi.URLParam(r, "nik")
	if nik == "" {
		http.Error(w, http.StatusText(422), 422)
		return
	}
	person, err := c.UC.GetPerson(nik)
	if err != nil {
		http.Error(w, fmt.Sprintf("person not found"), 404)
		return
	}

	json.NewEncoder(w).Encode(person)
}

func (c *PersonController) AddPersonController(w http.ResponseWriter, r *http.Request) {
	var person models.Person
	json.NewDecoder(r.Body).Decode(&person)
	if &person.NIK == nil || person.NIK == "" {
		http.Error(w, fmt.Sprintf("nik must required"), 422)
		return
	}
	val, err := c.UC.AddPerson(person)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte(val))
}

func (c *PersonController) SignInController(w http.ResponseWriter, r *http.Request) {
	var login models.Login
	json.NewDecoder(r.Body).Decode(&login)
	err := c.UC.SignIn(login.NIK, login.Password)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	token, err := common.GenerateToken(login.NIK)
	if err != nil {
		return
	}
	tokenString := fmt.Sprintf("Bearer %v", token)
	w.Header().Set("Authorization", tokenString)
	json.NewEncoder(w).Encode(token)
}

func (c *PersonController) DeletePersonController(w http.ResponseWriter, r *http.Request) {
	nik := chi.URLParam(r, "nik")
	if nik == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	err := c.UC.DeletePerson(nik)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("person successfully deleted"))
}

func (c *PersonController) UpdatePersonController(w http.ResponseWriter, r *http.Request) {
	nik := chi.URLParam(r, "nik")
	var person models.Person
	json.NewDecoder(r.Body).Decode(&person)
	if nik == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	err := c.UC.UpdatePerson(nik, person)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("person successfully updated"))
}
