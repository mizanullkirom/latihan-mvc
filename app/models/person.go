package models

// type Person struct {
// 	Name        string `json:"name" bson:"name"`
// 	NIK         string `json:"nik" bson:"nik"`
// 	PhoneNumber string `json:"phoneNumber" bson:"phoneNumber"`
// 	Address     string `json:"address" bson:"address"`
// }

type Person struct {
	Name        string `json:"name" bson:"name"`
	NIK         string `json:"nik" bson:"nik"`
	PhoneNumber string `json:"phoneNumber" bson:"phoneNumber"`
	Address     string `json:"address" bson:"address"`
	Password    string `json:"password" bson: "password"`
}

type Login struct {
	NIK      string `json : "nik"`
	Password string `json:"password"`
}

type PersonData struct {
	Name        string         `json:"name"`
	NIK         string         `json:"nik"`
	PhoneNumber string         `json:"phonenumber"`
	Address     string         `json:"address"`
	Items       []*PersonItems `json:"items"`
}

type PersonItems struct {
	Item string `json:"item" bson:"item"`
	Unit int    `json:"unit" bson:"unit"`
}
