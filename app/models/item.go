package models

type Item struct {
	NIK  string `json:"nik" bson:"nik"`
	Item string `json:"item" bson:"item"`
	Unit int    `json:"unit" bson:"unit"`
}
