package repository_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	"go.mongodb.org/mongo-driver/bson"
)

func TestAddItemMock(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	var reqTest = []struct {
		item    *models.Item
		wantErr error
	}{
		{
			&models.Item{
				NIK:  "1103130066",
				Item: "Seragam",
				Unit: 10,
			},
			nil,
		},
		{
			&models.Item{
				NIK:  "1103130066",
				Item: "Sepatu",
				Unit: 10,
			},
			nil,
		},
	}
	repo := &repository.Repository{}
	for _, testData := range reqTest {
		_, err := repo.AddItem(testData.item)
		asserts.Nil(err)
	}
}

func TestAddItem(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	item := models.Item{
		NIK:  "1103130066",
		Item: "Seragam",
		Unit: 10,
	}
	repo := &repository.Repository{}
	_, err := repo.AddItem(&item)
	asserts.Equal(err, nil)
}

func TestDeleteItem(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	_, err := repo.RemoveItem(bson.M{"nik": "1103130066", "item": "Seragam"})
	asserts.Equal(err, nil)
}
