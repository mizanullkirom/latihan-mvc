package repository

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repository struct{}

func (repo *Repository) GetPerson(p *models.Person, filter interface{}) error {

	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := collection.FindOne(ctx, filter).Decode(p)
	return err
}

func (repo *Repository) AddPerson(p *models.Person) (*mongo.InsertOneResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, p)
	return result, err
}

func (repo *Repository) UpdatePerson(filter interface{}, update interface{}) (*mongo.UpdateResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.UpdateMany(ctx, filter, update)
	return result, err
}

func (repo *Repository) RemovePerson(filter interface{}) (*mongo.DeleteResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	result, err := collection.DeleteOne(ctx, filter)
	return result, err

}

func (repo *Repository) SetPersonRedis(p *models.Person) error {
	rdb := common.GetRedis()
	ctx := context.Background()
	b, err := json.Marshal(p)
	if err != nil {
		return err
	}
	err = rdb.Set(ctx, p.NIK, string(b), 0).Err()
	err = rdb.Expire(ctx, p.NIK, 5*time.Minute).Err()
	return err
}

func (repo *Repository) GetPersonRedis(nik string) (string, error) {
	rdb := common.GetRedis()
	ctx := context.Background()
	val, err := rdb.Get(ctx, nik).Result()
	return val, err
}
