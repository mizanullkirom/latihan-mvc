package repository

import (
	"context"
	"log"
	"time"

	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	"go.mongodb.org/mongo-driver/mongo"
)

func (repo *Repository) GetItems(filter interface{}) []*models.PersonItems {
	mdb := common.GetMongo()
	collection := mdb.Collection("item")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	cur, err := collection.Find(ctx, filter)

	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	var results []*models.PersonItems
	for cur.Next(ctx) {
		item := &models.PersonItems{}
		er := cur.Decode(item)
		if er != nil {
			log.Fatal(er)
		}
		results = append(results, item)
	}
	return results
}

func (repo *Repository) GetItem(i *models.Item, filter interface{}) error {

	//Will automatically create a collection if not available
	mdb := common.GetMongo()
	collection := mdb.Collection("item")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := collection.FindOne(ctx, filter).Decode(i)
	return err
}

func (repo *Repository) UpdateItem(filter interface{}, update interface{}) (*mongo.UpdateResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("item")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.UpdateMany(ctx, filter, update)
	return result, err
}

func (repo *Repository) RemoveItem(filter interface{}) (*mongo.DeleteResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("item")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.DeleteOne(ctx, filter)
	return result, err
}

func (repo *Repository) AddItem(i *models.Item) (*mongo.InsertOneResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("item")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, i)
	return result, err
}
