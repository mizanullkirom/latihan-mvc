package repository

import (
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"go.mongodb.org/mongo-driver/mongo"
)

type IRepository interface {
	//person
	GetPerson(*models.Person, interface{}) error
	AddPerson(*models.Person) (*mongo.InsertOneResult, error)
	UpdatePerson(interface{}, interface{}) (*mongo.UpdateResult, error)
	RemovePerson(interface{}) (*mongo.DeleteResult, error)
	SetPersonRedis(*models.Person) error
	GetPersonRedis(string) (string, error)
	//item
	GetItems(interface{}) []*models.PersonItems
	GetItem(*models.Item, interface{}) error
	UpdateItem(interface{}, interface{}) (*mongo.UpdateResult, error)
	RemoveItem(interface{}) (*mongo.DeleteResult, error)
	AddItem(*models.Item) (*mongo.InsertOneResult, error)
}
