package usecase_test

import (
	"errors"
	"testing"

	"github.com/go-redis/redis/v8"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository/mocks"
	"gitlab.com/mizanullkirom/latihan-mvc/app/usecase"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	mock "gitlab.com/mizanullkirom/latihan-mvc/common/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestGetPersonMock(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockPerson := mocks.NewMockIRepository(mockCtrl)
	uc := &usecase.Usecase{Repo: mockPerson}

	person := &models.Person{}
	mockPerson.EXPECT().GetPerson(person, bson.M{"nik": "111"}).Return(nil).Times(1)
	mockPerson.EXPECT().GetPersonRedis("111").Return("", redis.Nil).Times(1)
	personItems := []*models.PersonItems{
		{Item: "sepatu", Unit: 5},
		{Item: "seragam", Unit: 5},
	}
	mockPerson.EXPECT().GetItems(bson.M{"nik": "111"}).Return(personItems).Times(1)
	_, err := uc.GetPerson("111")
	assert.Nil(t, err)
}

func TestGetPersonNotFoundMock(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockPerson := mocks.NewMockIRepository(mockCtrl)
	uc := &usecase.Usecase{Repo: mockPerson}

	person := &models.Person{}
	mockPerson.EXPECT().GetPerson(person, bson.M{"nik": "111"}).Return(errors.New("person not found")).Times(1)
	mockPerson.EXPECT().GetPersonRedis("111").Return("", redis.Nil).Times(1)
	_, err := uc.GetPerson("111")
	assert.Error(t, err, errors.New("person not found"))
}

func TestAddPersonMock(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockPerson := mocks.NewMockIRepository(mockCtrl)
	mockCrypto := mock.NewMockAppCrypto(mockCtrl)
	uc := &usecase.Usecase{
		Repo:   mockPerson,
		Crypto: mockCrypto,
	}
	personData := models.Person{
		Name:        "Test",
		NIK:         "00010010222",
		PhoneNumber: "085742666592",
		Address:     "Tegal",
		Password:    "test",
	}

	defaultCrypto := common.DefaultAppCrypto{}
	password := personData.Password
	hashed, err := defaultCrypto.EncryptString(password)
	if err != nil {
		return
	}
	personData.Password = string(hashed)
	existingPerson := &models.Person{}
	mockPerson.EXPECT().GetPerson(existingPerson, bson.M{"nik": personData.NIK}).Return(errors.New("error")).Times(1)
	mockPerson.EXPECT().SetPersonRedis(&personData).Return(nil).Times(1)
	mockCrypto.EXPECT().EncryptString(personData.Password).Return(hashed, nil).Times(1)
	mockPerson.EXPECT().AddPerson(&personData).Return(&mongo.InsertOneResult{}, nil).Times(1)

	val, err := uc.AddPerson(personData)
	assert.Nil(t, err)
	assert.Equal(t, val, "Person created successfully")
}

func TestAddItemMock(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockPerson := mocks.NewMockIRepository(mockCtrl)
	uc := &usecase.Usecase{Repo: mockPerson}

	person := &models.Person{}
	mockPerson.EXPECT().GetPerson(person, bson.M{"nik": "000"}).Return(nil).Times(1)
	existingItem := &models.Item{}
	mockPerson.EXPECT().GetItem(existingItem, bson.M{"nik": "000", "item": "sepatu"}).Return(errors.New("error")).Times(1)
	item := &models.Item{
		NIK:  "000",
		Item: "sepatu",
		Unit: 10,
	}
	mockPerson.EXPECT().AddItem(item).Return(&mongo.InsertOneResult{}, nil).Times(1)
	val, err := uc.AddItem(item)
	assert.Nil(t, err)
	assert.Equal(t, val, "Item Added")
}
