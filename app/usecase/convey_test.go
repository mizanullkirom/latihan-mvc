package usecase_test

import (
	"encoding/json"
	"errors"
	"testing"

	"github.com/go-redis/redis/v8"
	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository/mocks"
	"gitlab.com/mizanullkirom/latihan-mvc/app/usecase"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	mock "gitlab.com/mizanullkirom/latihan-mvc/common/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestPersonConvey(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockPerson := mocks.NewMockIRepository(mockCtrl)
	mockCrypto := mock.NewMockAppCrypto(mockCtrl)
	defer mockCtrl.Finish()

	personData := models.Person{
		Name:        "Test",
		NIK:         "00010010222",
		PhoneNumber: "085742666592",
		Address:     "Tegal",
		Password:    "test",
	}

	uc := &usecase.Usecase{
		Repo:   mockPerson,
		Crypto: mockCrypto,
	}

	Convey("AddPerson()", t, func() {
		defaultCrypto := common.DefaultAppCrypto{}
		password := personData.Password
		hashed, err := defaultCrypto.EncryptString(password)
		if err != nil {
			return
		}
		personData.Password = string(hashed)

		Convey("Create Person Failed", func() {

			Convey("When NIK is Empty", func() {
				personData.NIK = ""
				val, err := uc.AddPerson(personData)
				So(val, ShouldBeEmpty)
				ShouldEqual(err, "nik must required")
			})

			Convey("When Person Already Exist", func() {
				personData.NIK = "00010010222"
				existingPerson := &models.Person{}
				mockPerson.EXPECT().GetPerson(existingPerson, bson.M{"nik": personData.NIK}).Return(nil).Times(1)
				val, err := uc.AddPerson(personData)
				So(val, ShouldBeEmpty)
				ShouldEqual(err, "person already exist")
			})
		})

		Convey("Create Person Success", func() {
			Convey("When Create Succesfully", func() {
				existingPerson := &models.Person{}
				mockPerson.EXPECT().GetPerson(existingPerson, bson.M{"nik": personData.NIK}).Return(errors.New("error")).Times(1)
				mockCrypto.EXPECT().EncryptString(personData.Password).Return(hashed, nil).Times(1)
				mockPerson.EXPECT().SetPersonRedis(&personData).Return(nil).Times(1)
				mockPerson.EXPECT().AddPerson(&personData).Return(&mongo.InsertOneResult{}, nil).Times(1)
				val, err := uc.AddPerson(personData)
				So(err, ShouldBeNil)
				ShouldEqual(val, "Person created successfully")
			})
		})
	})

	Convey("GetPerson()", t, func() {
		Convey("When nik empty", func() {
			person, err := uc.GetPerson("")
			So(person, ShouldBeNil)
			ShouldEqual(err, "nik must required")
		})

		Convey("Redis Error Exception", func() {
			mockPerson.EXPECT().GetPersonRedis(personData.NIK).Return("", errors.New("error redis")).Times(1)
			person, err := uc.GetPerson(personData.NIK)
			So(person, ShouldBeNil)
			ShouldEqual(err, "error redis")
		})

		Convey("Get Data From Redis", func() {
			val := string(`{"name" : "test","nik" : "11031300066","phonenumber" : "0857339","address" : "tegal","items" : []}`)
			mockPerson.EXPECT().GetPersonRedis(personData.NIK).Return(val, nil).Times(1)
			person, err := uc.GetPerson(personData.NIK)
			data := &models.PersonData{}
			err = json.Unmarshal([]byte(val), data)
			So(err, ShouldBeNil)
			ShouldEqual(person, data)
		})

		Convey("GetPersonRedisNil", func() {
			person := &models.Person{}
			mockPerson.EXPECT().GetPerson(person, bson.M{"nik": personData.NIK}).Return(nil).Times(1)
			mockPerson.EXPECT().GetPersonRedis(personData.NIK).Return("", redis.Nil).Times(1)
			personItems := []*models.PersonItems{
				{Item: "sepatu", Unit: 5},
				{Item: "seragam", Unit: 5},
			}
			mockPerson.EXPECT().GetItems(bson.M{"nik": personData.NIK}).Return(personItems).Times(1)
			val, err := uc.GetPerson(personData.NIK)
			So(val, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

	})

	Convey("UpdatePerson()", t, func() {
		Convey("When NIK Empty", func() {
			person := models.Person{}
			err := uc.UpdatePerson("", person)
			ShouldEqual(err, "nik must required")
		})

		Convey("When Person NotFound", func() {
			person := &models.Person{}
			mockPerson.EXPECT().GetPerson(person, bson.M{"nik": personData.NIK}).Return(errors.New("")).Times(1)
			err := uc.UpdatePerson(personData.NIK, personData)
			ShouldEqual(err, "person not found")
		})

		Convey("When Update Success", func() {
			person := &models.Person{}
			mockPerson.EXPECT().GetPerson(person, bson.M{"nik": personData.NIK}).Return(nil).Times(1)
			mockPerson.EXPECT().UpdatePerson(bson.M{"nik": personData.NIK}, bson.M{"$set": personData}).Return(&mongo.UpdateResult{}, nil).Times(1)
			mockPerson.EXPECT().SetPersonRedis(&personData).Return(nil).Times(1)
			err := uc.UpdatePerson(personData.NIK, personData)
			So(err, ShouldBeNil)
		})
	})

	Convey("DeletePerson()", t, func() {
		Convey("When NIK Empty", func() {
			err := uc.DeletePerson("")
			ShouldEqual(err, "nik must required")
		})

		Convey("When Person NotFound", func() {
			person := &models.Person{}
			mockPerson.EXPECT().GetPerson(person, bson.M{"nik": personData.NIK}).Return(errors.New("")).Times(1)
			err := uc.DeletePerson(personData.NIK)
			ShouldEqual(err, "person not found")
		})

		Convey("Delete Success", func() {
			person := &models.Person{}
			mockPerson.EXPECT().GetPerson(person, bson.M{"nik": personData.NIK}).Return(nil).Times(1)
			mockPerson.EXPECT().RemovePerson(bson.M{"nik": personData.NIK}).Return(&mongo.DeleteResult{}, nil).Times(1)
			err := uc.DeletePerson(personData.NIK)
			So(err, ShouldBeNil)
		})
	})
}
