package usecase_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository"
	"gitlab.com/mizanullkirom/latihan-mvc/app/usecase"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
)

// func TestAddPersonMock(t *testing.T) {
// 	common.MongoInit("mongodb://127.0.0.1:27017", "test")
// 	common.RedisInit("localhost:6379", "")
// 	var reqTest = []struct {
// 		name    string
// 		person  models.Person
// 		wantErr error
// 	}{
// 		{
// 			"TestSuccess",
// 			models.Person{
// 				Name:        "Test",
// 				NIK:         "0001001022",
// 				PhoneNumber: "085742666592",
// 				Address:     "Tegal",
// 				Password:    "test1",
// 			},
// 			nil,
// 		},
// 		{
// 			"TestDuplicate",
// 			models.Person{
// 				Name:        "Test",
// 				NIK:         "0001001022",
// 				PhoneNumber: "085742666592",
// 				Address:     "Tegal",
// 				Password:    "test2",
// 			},
// 			errors.New("person already exist"),
// 		},
// 	}
// 	repo := &repository.Repository{}
// 	uc := &usecase.Usecase{
// 		Repo: repo,
// 	}
// 	for _, testData := range reqTest {
// 		t.Run(testData.name, func(t *testing.T) {
// 			asserts := assert.New(t)
// 			_, err := uc.AddPerson(testData.person)
// 			if err != nil {
// 				asserts.Error(err, testData.wantErr)
// 			}
// 			asserts.Nil(testData.wantErr)
// 		})
// 	}
// }
func TestAddPerson(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	common.RedisInit("localhost:6379", "")
	personData := models.Person{
		Name:        "Test",
		NIK:         "00010010",
		PhoneNumber: "085742666592",
		Address:     "Tegal",
	}

	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}

	val, err := uc.AddPerson(personData)
	asserts.NoError(err)
	asserts.Equal(val, "Person created successfully")
}

func TestAddPersonNikDuplicate(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	common.RedisInit("localhost:6379", "")
	personData := models.Person{
		Name:        "Test",
		NIK:         "00010010",
		PhoneNumber: "085742666592",
		Address:     "Tegal",
	}
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	_, err := uc.AddPerson(personData)
	asserts.Error(err, errors.New("person already exist"))

}

func TestGetPersonNotFound(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	common.RedisInit("localhost:6379", "")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	_, err := uc.GetPerson("000")
	asserts.Error(err, errors.New("person not found"))
}

func TestGetPersonWithoutNIK(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	_, err := uc.GetPerson("")
	asserts.Error(err, errors.New("nik must required"))
}

func TestGetPersonSuccess(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	common.RedisInit("localhost:6379", "")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	_, err := uc.GetPerson("1103130066")
	asserts.NoError(err)
}

func TestDeletePerson(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	err := uc.DeletePerson("00010010")
	asserts.Error(err, nil)
}

func TestDeleteItemNIKNotFound(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	err := uc.DeleteItem("321", "Sandal")
	asserts.Error(err, errors.New("person not found"))
}

func TestDeleteItem(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	err := uc.DeleteItem("1103130066", "Sepatu")
	fmt.Println(err)
	asserts.Error(err, nil)
}

func TestSignIn(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	err := uc.SignIn("586631", "duabelas")
	asserts.Error(err, nil)
}

func TestSignInNotFound(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	err := uc.SignIn("58663122", "duabelas")
	asserts.EqualError(err, "person not found")
}

func TestSignInPasswordNotMatch(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit("mongodb://127.0.0.1:27017", "test")
	repo := &repository.Repository{}
	uc := &usecase.Usecase{
		Repo: repo,
	}
	err := uc.SignIn("5866312", "duabelas333")
	asserts.EqualError(err, "password doesn't match")
}
