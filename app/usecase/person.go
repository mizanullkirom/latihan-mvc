package usecase

import (
	"encoding/json"
	"errors"

	"github.com/go-redis/redis/v8"
	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

type Usecase struct {
	Repo   repository.IRepository
	Crypto common.AppCrypto
}

func (uc *Usecase) GetPerson(nik string) (*models.PersonData, error) {
	if nik == "" {
		return nil, errors.New("nik must required")
	}
	person := &models.Person{}
	val, err := uc.Repo.GetPersonRedis(nik)
	if err == redis.Nil {
		err = uc.Repo.GetPerson(person, bson.M{"nik": nik})
		if err != nil {
			return nil, errors.New("person not found")
		}
		items := uc.Repo.GetItems(bson.M{"nik": nik})
		data := &models.PersonData{
			Name:        person.Name,
			NIK:         person.NIK,
			PhoneNumber: person.PhoneNumber,
			Address:     person.Address,
			Items:       items,
		}
		return data, nil
	} else if err != nil {
		return nil, err
	}
	data := &models.PersonData{}
	err = json.Unmarshal([]byte(val), data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (uc *Usecase) AddPerson(person models.Person) (string, error) {
	if person.NIK == "" {
		return "", errors.New("nik must required")
	}
	existingPerson := &models.Person{}
	err := uc.Repo.GetPerson(existingPerson, bson.M{"nik": person.NIK})
	if err == nil {
		return "", errors.New("person already exist")
	}
	password := person.Password
	val, err := uc.Crypto.EncryptString(password)
	if err != nil {
		return "", err
	}
	person.Password = string(val)
	err = uc.Repo.SetPersonRedis(&person)
	if err != nil {
		return "", err
	}
	_, err = uc.Repo.AddPerson(&person)
	if err != nil {
		return "", err
	}
	return "Person created successfully", nil
}

func (uc *Usecase) DeletePerson(nik string) error {
	if nik == "" {
		return errors.New("nik must required")
	}
	person := &models.Person{}
	err := uc.Repo.GetPerson(person, bson.M{"nik": nik})
	if err != nil {
		return errors.New("person not found")
	}
	_, err = uc.Repo.RemovePerson(bson.M{"nik": nik})
	return err
}

func (uc *Usecase) UpdatePerson(nik string, person models.Person) error {
	if nik == "" {
		return errors.New("nik must required")
	}
	existingperson := &models.Person{}
	err := uc.Repo.GetPerson(existingperson, bson.M{"nik": nik})
	if err != nil {
		return errors.New("person not found")
	}
	_, err = uc.Repo.UpdatePerson(bson.M{"nik": nik}, bson.M{"$set": person})
	if err != nil {
		return err
	}
	err = uc.Repo.SetPersonRedis(&person)
	if err != nil {
		return err
	}
	return nil
}

func (uc *Usecase) SignIn(nik string, password string) error {
	existingperson := &models.Person{}
	err := uc.Repo.GetPerson(existingperson, bson.M{"nik": nik})
	if err != nil {
		return errors.New("person not found")
	}
	err = uc.Crypto.CheckPassword(existingperson.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return errors.New("password doesn't match")
	}
	return err
}
