package usecase

import (
	"errors"
	"fmt"
	"log"

	"gitlab.com/mizanullkirom/latihan-mvc/app/models"
	"go.mongodb.org/mongo-driver/bson"
)

func (uc *Usecase) AddItem(item *models.Item) (string, error) {
	existingPerson := &models.Person{}
	err := uc.Repo.GetPerson(existingPerson, bson.M{"nik": item.NIK})
	if err != nil {
		return "", errors.New("person not found , insert person first")
	}
	existingItem := &models.Item{}
	err = uc.Repo.GetItem(existingItem, bson.M{"nik": item.NIK, "item": item.Item})
	if err == nil {
		total := existingItem.Unit + item.Unit
		existingItem.Unit = total
		_, err := uc.Repo.UpdateItem(bson.M{"nik": item.NIK, "item": item.Item}, bson.M{"$set": existingItem})
		if err != nil {
			fmt.Println(err)
			return "", err
		}
		return "Item Added", nil
	}
	_, err = uc.Repo.AddItem(item)
	if err != nil {
		return "", err
	}
	return "Item Added", nil
}

func (uc *Usecase) DeleteItem(nik, item string) error {
	_, err := uc.Repo.RemoveItem(bson.M{"nik": nik, "item": item})
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}
