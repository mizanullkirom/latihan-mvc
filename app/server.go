package app

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	controller "gitlab.com/mizanullkirom/latihan-mvc/app/controllers"
	"gitlab.com/mizanullkirom/latihan-mvc/app/repository"
	"gitlab.com/mizanullkirom/latihan-mvc/app/usecase"
	"gitlab.com/mizanullkirom/latihan-mvc/common"
)

type Server struct {
	Router *chi.Mux
}

type AppConfig struct {
	AppPort string
}

type MongoConfig struct {
	MongoHost string
	MongoPort string
	Mongodb   string
}

type RedisConfig struct {
	RedisHost     string
	RedisPort     string
	RedisPassword string
}

type Controller struct {
	Person *controller.PersonController
}

var ctrl = &Controller{}

func PersonCtrlInit() {
	repository := repository.Repository{}
	cryto := common.DefaultAppCrypto{}
	uc := &usecase.Usecase{
		Repo:   &repository,
		Crypto: cryto,
	}
	personCtrl := &controller.PersonController{UC: uc}
	ctrl.Person = personCtrl
}

func (server *Server) Initialize() {
	server.initializeRoutes()
}

func (server *Server) Run(addr string) {
	fmt.Printf("Listening to port %s", addr)
	log.Fatal(http.ListenAndServe(addr, server.Router))
}

func getEnv(key, def string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return def
}

func Run() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	server := Server{}
	appConf := AppConfig{}
	mongoConf := MongoConfig{}
	redisConf := RedisConfig{}

	// appllication config
	appConf.AppPort = getEnv("APP_PORT", "3000")
	// mongo config
	mongoConf.MongoHost = getEnv("MONGO_HOST", "localhost")
	mongoConf.MongoPort = getEnv("MONGO_PORT", "27017")
	mongoConf.Mongodb = getEnv("MONGO_DB", "test")
	// redis config
	redisConf.RedisHost = getEnv("REDIS_HOST", "localhost")
	redisConf.RedisPassword = getEnv("REDIS_PASSWORD", "")
	redisConf.RedisPort = getEnv("REDIS_PORT", "6379")

	mongoAddress := "mongodb://" + mongoConf.MongoHost + ":" + mongoConf.MongoPort
	redisAddress := redisConf.RedisHost + ":" + redisConf.RedisPort

	common.RedisInit(redisAddress, redisConf.RedisPassword)
	common.MongoInit(mongoAddress, mongoConf.Mongodb)
	server.Initialize()
	server.Run(":" + appConf.AppPort)
}
