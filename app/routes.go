package app

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/mizanullkirom/latihan-mvc/app/middleware"
)

func (server *Server) initializeRoutes() {
	server.Router = chi.NewRouter()
	m := middleware.NewMiddleware("serviceName")

	server.Router.Use(m)
	server.Router.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300,
	}))

	PersonCtrlInit()
	server.Router.Handle("/metrics", promhttp.Handler())
	server.Router.With(middleware.SetMiddlewareJSON).Post("/login", ctrl.Person.SignInController)
	server.Router.Route("/person", func(r chi.Router) {
		// r.Use(middleware.Authentication)
		r.Get("/{nik}", ctrl.Person.GetPersonController)
		r.Post("/", ctrl.Person.AddPersonController)
		r.Delete("/{nik}", ctrl.Person.DeletePersonController)
		r.Put("/{nik}", ctrl.Person.UpdatePersonController)
	})
	server.Router.Route("/item", func(r chi.Router) {
		r.Post("/", ctrl.Person.AddItemController)
		r.Delete("/{nik}/{item}", ctrl.Person.DeleteItemController)
	})
}
